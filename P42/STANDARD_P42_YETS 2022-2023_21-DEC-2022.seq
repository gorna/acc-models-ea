

  /**********************************************************************************
  *
  * P42 version (draft) YETS 2022-2023 in MAD X SEQUENCE format
  * Generated the 21-DEC-2022 16:27:36 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.P42_BBS                      := 0.45;
l.P42_BBS_TBIU                 := 0;
l.P42_BSGV                     := 0.45;
l.P42_BSI                      := 0;
l.P42_BSI_TBIU                 := 0;
l.P42_BSM                      := 0.45;
l.P42_BSP                      := 0.45;
l.P42_BSP_TBIU                 := 0;
l.P42_MBNH_HWP                 := 5;
l.P42_MBNV_HWP                 := 5;
l.P42_MBW__HWP                 := 6.2;
l.P42_MBXGDCWP                 := 3;
l.P42_MCXCAHWC                 := 0.4;
l.P42_MDSH                     := 0.7;
l.P42_MSN                      := 3.2;
l.P42_MTN                      := 3.6;
l.P42_OMK                      := 0;
l.P42_QNL__8WP                 := 2.99;
l.P42_QSL__8WP                 := 3;
l.P42_TBID                     := 0.25;
l.P42_TBIU                     := 0;
l.P42_TCMAA                    := 0.4;
l.P42_TCMAC                    := 0.4;
l.P42_XCHV_001                 := 1;
l.P42_XCIOP001                 := 0.1;
l.P42_XFFH                     := 0.276;
l.P42_XFFV                     := 0.276;
l.P42_XTAX_007                 := 1.615;
l.P42_XTAX_008                 := 1.615;
l.P42_XTCX_005                 := 1.6;
l.P42_XTCX_006                 := 2.4;
l.P42_XVWAC001                 := 0.001;
l.P42_XVWAD001                 := 0.001;
l.P42_XVWAE001                 := 0.002;

//---------------------- COLLIMATOR     ---------------------------------------------
P42_TCMAA      : COLLIMATOR  , L := l.P42_TCMAA;         ! Collimation mask type A
P42_TCMAC      : COLLIMATOR  , L := l.P42_TCMAC;         ! Collimation mask type C
P42_XCHV_001   : COLLIMATOR  , L := l.P42_XCHV_001;      ! SPS Collimator horizontal et vertical 4 blocks (design 1970)
P42_XCIOP001   : COLLIMATOR  , L := l.P42_XCIOP001;      ! Converter IN OUT Plate - Lead
P42_XTCX_005   : COLLIMATOR  , L := l.P42_XTCX_005;      ! XTCX - Fixed Collimator 1.6 m, Vacuum, no cooling
P42_XTCX_006   : COLLIMATOR  , L := l.P42_XTCX_006;      ! XTCX - Fixed collimator Type 006
//---------------------- HKICKER        ---------------------------------------------
P42_MDSH       : HKICKER     , L := l.P42_MDSH;          ! Correcting dipole, BT line, short, horizontal deflection
P42_MSN        : RBEND     , L := l.P42_MSN;           ! Septum magnet, north area - offset mechanical and optical dimensions according to drawing 1766138 (not well indicated)
//---------------------- INSTRUMENT     ---------------------------------------------
P42_BBS        : INSTRUMENT  , L := l.P42_BBS;           ! Beam Box Scanner
P42_BBS_TBIU   : INSTRUMENT  , L := l.P42_BBS_TBIU;      ! Beam Box Scanner (special inside TBIU)
P42_BSGV       : INSTRUMENT  , L := l.P42_BSGV;          ! SEM grid, vertical
P42_BSI        : INSTRUMENT  , L := l.P42_BSI;           ! SEM intensity
P42_BSI_TBIU   : INSTRUMENT  , L := l.P42_BSI_TBIU;      ! SEM intensity (special Inside TBIU)
P42_BSM        : INSTRUMENT  , L := l.P42_BSM;           ! SEM position, moveable
P42_TBID       : INSTRUMENT  , L := l.P42_TBID;          ! target beam instrumentation, downstream
P42_TBIU       : INSTRUMENT  , L := l.P42_TBIU;          ! target beam instrumentation, upstream
P42_XFFH       : INSTRUMENT  , L := l.P42_XFFH;          ! Finger Scintillator Profile Monitor - Horizontal
P42_XFFV       : INSTRUMENT  , L := l.P42_XFFV;          ! Finger Scintillator Profile Monitor - Vertical
P42_XTAX_007   : INSTRUMENT  , L := l.P42_XTAX_007;      ! Target Absorber Type 007
P42_XTAX_008   : INSTRUMENT  , L := l.P42_XTAX_008;      ! Target Absorber Type 008
P42_XVWAC001   : INSTRUMENT  , L := l.P42_XVWAC001;      ! X Vacuum Window Aluminium DN159X0.1, conical flanges
P42_XVWAD001   : INSTRUMENT  , L := l.P42_XVWAD001;      ! X Vacuum Window Aluminium DN159X0.1, aperture 120 mm
P42_XVWAE001   : INSTRUMENT  , L := l.P42_XVWAE001;      ! X Vacuum Window Aluminium EL900X60X0.2
//---------------------- KICKER         ---------------------------------------------
P42_MCXCAHWC   : KICKER      , L := l.P42_MCXCAHWC;      ! Corrector magnet, H or V, type MDX
//---------------------- MARKER         ---------------------------------------------
P42_OMK        : MARKER      , L := l.P42_OMK;           ! P42 markers
//---------------------- MONITOR        ---------------------------------------------
P42_BSP        : MONITOR     , L := l.P42_BSP;           ! SEM position, horizontal + vertical
P42_BSP_TBIU   : MONITOR     , L := l.P42_BSP_TBIU;      ! SEM position, horizontal + vertical (special inside TBIU)
//---------------------- QUADRUPOLE     ---------------------------------------------
P42_QNL__8WP   : QUADRUPOLE  , L := l.P42_QNL__8WP;      ! Quadrupole, secondary beams, type north area
P42_QSL__8WP   : QUADRUPOLE  , L := l.P42_QSL__8WP;      ! Quadrupole, slim, long, - Same magnet type SPQSLD_
//---------------------- RBEND          ---------------------------------------------
P42_MBNH_HWP   : RBEND       , L := l.P42_MBNH_HWP;      ! Bending magnet, secondary beams, horizontal, north area
P42_MBNV_HWP   : RBEND       , L := l.P42_MBNV_HWP;      ! Bending magnet, secondary beams, vertical, north area - Mechanical dimensions from NORMA
P42_MBW__HWP   : RBEND       , L := l.P42_MBW__HWP;      ! Bending Magnet, main, type B2, 1000A
P42_MBXGDCWP   : RBEND       , L := l.P42_MBXGDCWP;      ! Bending Magnet, H or V, type MCW
P42_MTN        : RBEND       , L := l.P42_MTN;           ! Bending magnet, Target N


/************************************************************************************/
/*                       P42 SEQUENCE                                               */
/************************************************************************************/

P42 : SEQUENCE, refer = centre,         L = 838.16;
 TBACA.X0400000                : P42_OMK         , at = 0            , slot_id = 56964667;
 TBID.241150                   : P42_TBID        , at = .475         , slot_id = 47534194;
 TCMAA.X0400001                : P42_TCMAA       , at = .85          , slot_id = 56992277;
 XVW.X0430001                  : P42_XVWAD001    , at = 1.1005       , slot_id = 57310366;
 MTN.X0400003                  : P42_MTN         , at = 3.15         , slot_id = 56992202;
 MTN.X0400007                  : P42_MTN         , at = 7.35         , slot_id = 56992225;
 XVW.X0430017                  : P42_XVWAE001    , at = 16.612000147 , slot_id = 57310461;
 XTAX.X0430018                 : P42_XTAX_007    , at = 18.0075      , slot_id = 57087670;
 XTAX.X0430020                 : P42_XTAX_008    , at = 19.6325      , slot_id = 56992724;
 XCIO.X0430021                 : P42_XCIOP001    , at = 20.525       , slot_id = 56995006;
 XVW.X0430020                  : P42_XVWAD001    , at = 20.604500147 , slot_id = 57310475;
 MSN.X0430022                  : P42_MSN         , at = 22.45        , slot_id = 56992358;
 MSN.X0430029                  : P42_MSN         , at = 29.43        , slot_id = 56992392;
 QSL.X0430033                  : P42_QSL__8WP    , at = 32.92        , slot_id = 56992417;
 QNL.X0430040                  : P42_QNL__8WP    , at = 39.78        , slot_id = 56050728;
 XTCX.X0430042                 : P42_XTCX_005    , at = 42.425       , slot_id = 57087872;
 MCXCA.X0430048                : P42_MCXCAHWC    , at = 48.095       , slot_id = 56051257;
 QNL.X0430050                  : P42_QNL__8WP    , at = 50.28        , slot_id = 56050737;
 QNL.X0430054                  : P42_QNL__8WP    , at = 53.71        , slot_id = 56050746;
 QNL.X0430064                  : P42_QNL__8WP    , at = 64.21        , slot_id = 56050755;
 MBW.X0430069                  : P42_MBW__HWP    , at = 69.219       , slot_id = 40400557;
 MBW.X0430076                  : P42_MBW__HWP    , at = 75.859       , slot_id = 56050692;
 MBW.X0430082                  : P42_MBW__HWP    , at = 82.499       , slot_id = 40401984;
 MBW.X0430089                  : P42_MBW__HWP    , at = 89.139       , slot_id = 56050701;
 MBW.X0430096                  : P42_MBW__HWP    , at = 95.779       , slot_id = 40401985;
 MCXCA.X0430100                : P42_MCXCAHWC    , at = 99.56        , slot_id = 56050791;
 QNL.X0430108                  : P42_QNL__8WP    , at = 107.75       , slot_id = 56050764;
 QNL.X0430111                  : P42_QNL__8WP    , at = 111.18       , slot_id = 56050773;
 QNL.X0430169                  : P42_QNL__8WP    , at = 168.65       , slot_id = 56994620;
 MCXCA.X0430171                : P42_MCXCAHWC    , at = 170.7        , slot_id = 56994644;
 BBS.X0430224                  : P42_BBS         , at = 223.46       , slot_id = 57088220;
 BSP.X0430225                  : P42_BSP         , at = 224.045      , slot_id = 57088055;
 QNL.X0430226                  : P42_QNL__8WP    , at = 226.12       , slot_id = 56994696;
 MCXCA.X0430228                : P42_MCXCAHWC    , at = 228.17       , slot_id = 56994723;
 BBS.X0430281                  : P42_BBS         , at = 280.93       , slot_id = 57088229;
 BSP.X0430282                  : P42_BSP         , at = 281.515      , slot_id = 57088064;
 BSI.X0430282                  : P42_BSI         , at = 281.74       , slot_id = 57088293;
 QNL.X0430284                  : P42_QNL__8WP    , at = 283.59       , slot_id = 56994981;
 XCHV.X0430286                 : P42_XCHV_001    , at = 286.015      , slot_id = 56995032;
 QNL.X0430341                  : P42_QNL__8WP    , at = 341.06       , slot_id = 56995143;
 XVW.X0430388                  : P42_XVWAC001    , at = 388.544      , slot_id = 57311850;
 BSG.X0430389                  : P42_BSGV        , at = 388.922      , slot_id = 57311266;
 XVW.X0430390                  : P42_XVWAC001    , at = 390.206      , slot_id = 57311904;
 XCHV.X0430396                 : P42_XCHV_001    , at = 396.105      , slot_id = 56997983;
 QNL.X0430399                  : P42_QNL__8WP    , at = 398.53       , slot_id = 56998033;
 MDSH.X0430401                 : P42_MDSH        , at = 400.789      , slot_id = 56998114;
 XCHV.X0430454                 : P42_XCHV_001    , at = 453.575      , slot_id = 56997992;
 QNL.X0430456                  : P42_QNL__8WP    , at = 456          , slot_id = 56998042;
 MCXCA.X0430458                : P42_MCXCAHWC    , at = 458.05       , slot_id = 56998123;
 XVW.X0430509                  : P42_XVWAC001    , at = 509.942      , slot_id = 57311953;
 BSG.X0430510                  : P42_BSGV        , at = 510.32       , slot_id = 57311215;
 XVW.X0430511                  : P42_XVWAC001    , at = 511.604      , slot_id = 57312002;
 QNL.X0430513                  : P42_QNL__8WP    , at = 513.47       , slot_id = 56998051;
 XFFV.X0430515                 : P42_XFFV        , at = 515.323      , slot_id = 56998177;
 XFFH.X0430516                 : P42_XFFH        , at = 515.599      , slot_id = 56998159;
 MBNH.X0430520                  : P42_MBNH_HWP    , at = 519.565      , slot_id = 56998186;
 MBNH.X0430525                  : P42_MBNH_HWP    , at = 525.225      , slot_id = 56998195;
 MBNH.X0430531                  : P42_MBNH_HWP    , at = 530.885      , slot_id = 56998204;
 MBNH.X0430537                  : P42_MBNH_HWP    , at = 536.545      , slot_id = 56998213;
 MBNH.X0430542                  : P42_MBNH_HWP    , at = 542.205      , slot_id = 56998222;
 MBNH.X0430548                  : P42_MBNH_HWP    , at = 547.865      , slot_id = 56998231;
 MBNH.X0430554                  : P42_MBNH_HWP    , at = 553.525      , slot_id = 56998240;
 MBNH.X0430559                  : P42_MBNH_HWP    , at = 559.185      , slot_id = 56998249;
 MBNH.X0430565                 : P42_MBNH_HWP    , at = 564.845      , slot_id = 56998258;
 BSP.X0430569                  : P42_BSP         , at = 568.865      , slot_id = 57088073;
 QNL.X0430571                  : P42_QNL__8WP    , at = 570.94       , slot_id = 56998060;
 MCXCA.X0430573                : P42_MCXCAHWC    , at = 572.99       , slot_id = 56998132;
 BSP.X0430626                  : P42_BSP         , at = 626.335      , slot_id = 57088082;
 QNL.X0430628                  : P42_QNL__8WP    , at = 628.41       , slot_id = 56998069;
 XTCX.X0430631                 : P42_XTCX_006    , at = 631.455      , slot_id = 57087897;
 BSG.X0430655                  : P42_BSGV        , at = 654.977      , slot_id = 57311360;
 XCHV.X0430683                 : P42_XCHV_001    , at = 683.455      , slot_id = 56998003;
 QNL.X0430685                  : P42_QNL__8WP    , at = 685.88       , slot_id = 56998078;
 MCXCA.X0430688                : P42_MCXCAHWC    , at = 687.93       , slot_id = 56998141;
 XFFH.X0430708                 : P42_XFFH        , at = 706.882      , slot_id = 57198319;
 QNL.X0430710                  : P42_QNL__8WP    , at = 708.87       , slot_id = 57196968;
 QNL.X0430714                  : P42_QNL__8WP    , at = 712.915      , slot_id = 56998087;
 MCXCA.X0430715                : P42_MCXCAHWC    , at = 714.965      , slot_id = 56998150;
 MBNH.X0430718                 : P42_MBNH_HWP    , at = 718.23       , slot_id = 56998267;
 MBNH.X0430724                 : P42_MBNH_HWP    , at = 723.89       , slot_id = 56998276;
 MBNH.X0430730                 : P42_MBNH_HWP    , at = 729.55       , slot_id = 56998285;
 MBNH.X0430735                 : P42_MBNH_HWP    , at = 735.21       , slot_id = 56998294;
 BSP.X0450769                  : P42_BSP         , at = 768.31       , slot_id = 57088184;
 BSI.X0450769                  : P42_BSI         , at = 768.535      , slot_id = 57088316;
 QNL.X0450770                  : P42_QNL__8WP    , at = 770.385      , slot_id = 56050809;
 QNL.X0450792                  : P42_QNL__8WP    , at = 792.125      , slot_id = 56050818;
 QNL.X0450795                  : P42_QNL__8WP    , at = 795.555      , slot_id = 56050827;
 QNL.X0450814                  : P42_QNL__8WP    , at = 813.865      , slot_id = 56050836;
 QNL.X0450817                  : P42_QNL__8WP    , at = 817.295      , slot_id = 56051237;
 BSM.X0450819                  : P42_BSM         , at = 819.235      , slot_id = 57212612;
 MCXCA.X0450820                : P42_MCXCAHWC    , at = 819.895      , slot_id = 56051246;
 MBNV.X0450823                 : P42_MBNV_HWP    , at = 823.06       , slot_id = 57087563;
 MBNV.X0450829                 : P42_MBNV_HWP    , at = 828.72       , slot_id = 57087572;
 XVW.X0450831                  : P42_XVWAC001    , at = 831.703      , slot_id = 57312051;
 MBXGD.X0450834                : P42_MBXGDCWP    , at = 834.428      , slot_id = 56050800;
 BSG.X0450836                  : P42_BSGV        , at = 836.504      , slot_id = 57311552;
 XVW.X0450837                  : P42_XVWAC001    , at = 836.882      , slot_id = 57312100;
 BBS.X0450837                  : P42_BBS_TBIU    , at = 836.95       , slot_id = 57311185, assembly_id= 56964638;
 BSI.X0450837                  : P42_BSI_TBIU    , at = 836.95       , slot_id = 57311151, assembly_id= 56964638;
 BSP.X0450837                  : P42_BSP_TBIU    , at = 836.95       , slot_id = 57311119, assembly_id= 56964638;
 TBIU.X0450837                 : P42_TBIU        , at = 837.1        , slot_id = 56964638;
 TCMAC.X0450838                : P42_TCMAC       , at = 837.5        , slot_id = 56993160;
 TBACB.X1010000                : P42_OMK         , at = 838.06       , slot_id = 55490825;
ENDSEQUENCE;

/************************************************************************************/
/*                       STRENGTHS                                                  */
/************************************************************************************/

//---------------------------- Strength Definitions ----------------------------- 
MTN.X0400003, ANGLE := kB1B3_T4, TILT=-0.0002617993877991494;
MTN.X0400007, ANGLE := kB1B3_T4, TILT=-0.00026205145192120815;
MSN.X0430022, ANGLE := kB2, TILT=-0.00026230364426381447;
MSN.X0430029, ANGLE := kB3, TILT=-0.00026179938820641385;
MCXCA.X0430048, HKICK := kTRIM1, TILT=1.5705355343774487;
MBW.X0430069, ANGLE := kB4, TILT=-0.3693106627516487;
MBW.X0430076, ANGLE := kB4, TILT=-0.3693157743395632;
MBW.X0430082, ANGLE := kB5, TILT=-0.00028565810790937403;
MBW.X0430089, ANGLE := kB4, TILT=-0.369364566576347;
MBW.X0430096, ANGLE := kB4, TILT=-0.36939897325455306;
MCXCA.X0430100, HKICK := kTRIM2, TILT=1.570398174455684;
MCXCA.X0430171, HKICK := kTRIM3, TILT=-0.0003981523392124264;
MCXCA.X0430228, HKICK := kTRIM4, TILT=1.570398174455684;
MDS.X0430401, HKICK := kTRIM5, TILT=-0.00039815233921242674;
MCXCA.X0430458, HKICK := kTRIM6, TILT=1.570398174455684;
MBNH.X0430520, ANGLE := kB71, TILT=-0.00039815233921242706;
MBNH.X0430525, ANGLE := kB72, TILT=-0.000343169583121045;
MBNH.X0430531, ANGLE := kB71, TILT=-0.0002881972412966894;
MBNH.X0430537, ANGLE := kB72, TILT=-0.0002332353135584029;
MBNH.X0430542, ANGLE := kB71, TILT=-0.00017828379970740522;
MBNH.X0430548, ANGLE := kB72, TILT=-0.00012334269956141007;
MBNH.X0430554, ANGLE := kB71, TILT=-6.841201293791084e-05;
MBNH.X0430559, ANGLE := kB72, TILT=-1.3491739646258056e-05;
MBNH.X0430565, ANGLE := kB71, TILT=4.141812050438982e-05;
MCXCA.X0430573, HKICK := kTRIM7, TILT=1.5708926443625928;
MCXCA.X0430688, HKICK := kTRIM8, TILT=9.631756769629276e-05;
MCXCA.X0430715, HKICK := kTRIM9, TILT=1.5708926443625928;
MBNH.X0430718, ANGLE := kB8, TILT=9.631756769629205e-05;
MBNH.X0430724, ANGLE := kB8, TILT=0.00016168786617705617;
MBNH.X0430730, ANGLE := kB9, TILT=0.00022704502762330343;
MBNH.X0430735, ANGLE := kB9, TILT=0.00020012204054811695;
MCXCA.X0450820, HKICK := kTRIM10, TILT=0.0001731988014928568;
MBNV.X0450823, ANGLE := kB10, TILT=1.5709695255963894;
MBNV.X0450829, ANGLE := kB11, TILT=1.5709695152696603;
MBXGD.X0450834, ANGLE := kB12, TILT=1.5709695165778923;

return;